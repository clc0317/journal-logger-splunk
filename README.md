journald-logger
===============

A Docker image to install the journald-logger service, which logs journalctl logs to disk as json, so splunk can parse them.

Based on [this blog post on splunk.com](https://www.splunk.com/blog/2015/04/30/integrating-splunk-with-docker-coreos-and-journald.html) about integrating containerized Splunk forwarders.

**CONFIGURATION**

The Unit file parses /etc/sysconfig/journald-logger for two variables:

* MAX_JOURNAL_FILE_SIZE=10M
* JOURNAL_FILE=/var/srv/journal

Customization can be done per host, after that file is installed by the container, or before building the image so every host running the image will get the desired configuration.

**BUILD**

`docker build -t journald-logger .`

**INSTALL**

With the Atomic cli:

`atomic install journald-logger`

With vanilla Docker:

`$(docker inspect --format='{{.Config.Labels.INSTALL}}' journald-logger)`

**UNINSTALL**

With the Atomic cli:

`atomic uninstall journald-logger`

With vanilla Docker:

`$(docker inspect --format='{{.Config.Labels.UNINSTALL}}' journald-logger)`

**SPLUNK SETUP**

The Splunk forwarder can be configured by modifying the `inputs.conf`, `outputs.conf` and `props.conf` files:

_inputs.conf_

```
[monitor:/var/srv/journal]
sourcetype = journald
```

_outputs.conf_

```
[tcpout]
defaultGroup = default-autolb-group[tcpout:default-autolb-group]
server = indexer.example.com:9997[tcpout-server://indexer.example.com:9997]
```

_props.conf_

```
[journald]
KV_MODE = json
MAX_TIMESTAMP_LOOKAHEAD = 10
NO_BINARY_CHECK = 1
SHOULD_LINEMERGE = false
TIME_FORMAT = %s
TIME_PREFIX = \"__REALTIME_TIMESTAMP\" : \"
pulldown_type = 1
TZ=UTC
```



