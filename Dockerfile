FROM centos:centos7
LABEL maintainer Chris Collins <christopher.collins@duke.edu>

LABEL INSTALL="docker run --rm --privileged -v /:/host:z -e HOST=/host \${IMAGE} /bin/install.sh"
LABEL UNINSTALL="docker run --rm --privileged -v /:/host:z -e HOST=/host \${IMAGE} /bin/uninstall.sh"

ADD journald-logger.service /opt
ADD journald-logger /opt

ADD install.sh /bin
ADD uninstall.sh /bin

RUN chmod +x /bin/install.sh /bin/uninstall.sh
