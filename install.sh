#!/bin/bash

SERVICE=journald-logger.service
SYSCONFIG=journald-logger

HOSTPATH=${HOST}

if [[ -z $HOSTPATH ]]
then
  echo "Host path unset"
  exit 1
fi

cp /opt/${SERVICE} ${HOST}/etc/systemd/system/
cp /opt/${SYSCONFIG} ${HOST}/etc/sysconfig/
chroot ${HOST} ln -s /etc/systemd/system/journald-logger.service /etc/systemd/system/multi-user.target.wants/journald-logger.service
