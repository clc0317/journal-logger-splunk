#!/bin/bash

SERVICE=journald-logger.service
SYSCONFIG=journald-logger

HOSTPATH=${HOST}

if [[ -z $HOSTPATH ]]
then
  echo "Host path unset"
  exit 1
fi

rm -f ${HOST}/etc/systemd/system/${SERVICE}
rm -f ${HOST}/etc/sysconfig/${SYSCONFIG}
rm -f ${HOST}/etc/systemd/system/multi-user.target.wants/journald-logger.service
